﻿$ErrorActionPreference = 'Stop'; # stop on all errors

$packageName= 'millennium3' # arbitrary name for the package, used in messages
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://sapdpmgmtdata.blob.core.windows.net/pubpackages/Millenn329100.exe' # download url

$packageArgs = @{
  packageName   = $packageName
  fileType      = 'exe'
  url           = $url
  silentArgs   = '/s /v"/qn"'
  softwareName  = 'Millennium 3'
}

Install-ChocolateyPackage @packageArgs
